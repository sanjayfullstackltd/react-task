export const storeRepoData = (data) =>{
    return {
        type:"RepoData",
        payload:data
    }
}
export const filterRepoData = (data) =>{
    return {
        type:"FilterRepoData",
        payload:data
    }
}
export const storeDevData = (data) =>{
    return {
        type:"DevData",
        payload:data
    }
}
