const initialState ={
    repoData:[],
    devData:[],
    filterRepoData:[],
}
const CountReduser = (state=initialState,action) =>{
    switch(action.type){
        case "RepoData": 
            return{
                ...state,
                repoData : action.payload,
            }
        case "DevData": 
            return{
                ...state,
                devData : action.payload,
            }
        case "FilterRepoData":
            return{
                ...state,
                filterRepoData : action.payload,
            }
        default : 
            return state
    }
}

export default CountReduser;
