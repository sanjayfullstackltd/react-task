import React, { useEffect, useRef, useState, Fragment } from "react";

import { Link, useHistory } from "react-router-dom";
import Document from "./../../assets/images/document.svg";
import Headers from "./../Header/Header";
import star from "./../../assets/images/star.svg";
import flame from "./../../assets/images/flame.svg";
import userImage from "./../../assets/images/userimage.jpg";
import { useSelector } from "react-redux";

function Developer(props) {
  const history = useHistory();
  const developerData = useSelector(state => state.AllData.devData);
  const [devData, SetdevData] = useState(developerData);

  useEffect(() => {
    SetdevData(developerData)
  }, [developerData]);
  return (
    <div>
      <div className="devloper-page">
        <div className="container-fluid">
          <div className="row">
            <div className="header-block">
              <div className="section-title text-center">
                <h1>Trending</h1>
                <p>
                  See what the Github community is most excited about today.
                </p>
              </div>
            </div>
            <div className="mx-auto col-lg-6">
              <div className="listing-block">
                <Headers tabName="developer" />

                {devData.length > 0 ?
                  devData.map((cur) => {
                    return (
                      <div class="listing devloper-list">
                        <div class="row">
                          <div className="col-md-6 details">
                            <div className="title">
                              <span className="title-icon">{cur.rank}</span>
                              <div className="d-flex">
                                <span className="userimage">
                                  <img src={cur.avatar} />
                                </span>
                                <div className="py-2 ps-2">
                                  <p className="title">{cur.name}</p>
                                  <p>{cur.username}</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-4 align-items-start flex-column d-flex">
                            <Link className="star">
                              <span className="star-icon">
                                <img src={flame} />
                              </span>
                              Popular Repository
                            </Link>
                            <a
                              className="star"
                              href={cur.popularRepository.url}
                              target="_blank"
                            >
                              {cur.popularRepository?.repositoryName && (
                                <div className="d-flex align-items-center">
                                  <span className="star-icon">
                                    <img src={Document} />
                                  </span>
                                  <span className="reponame">
                                    {cur.popularRepository.repositoryName}
                                  </span>
                                </div>
                              )}
                            </a>
                            <p className="description">
                              {cur.popularRepository?.description}
                            </p>
                          </div>
                          <div className="col-md-2 text-end">
                            <Link className="star">
                              <span className="star-icon">
                                <img src={star} />
                              </span>
                              Star
                            </Link>
                          </div>
                        </div>
                      </div>
                    );
                  })
                :
                  <p>Loading...</p>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Developer;
