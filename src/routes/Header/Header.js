import React, { useEffect, useRef, useState, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { filterRepoData } from "../../redux/actions";

function Header(props) {
  const history = useHistory();
  const dispatch = useDispatch();
  const currentRef = useRef();
  const repositoryData = useSelector(state => state.AllData.repoData);
  const [repoData, setRepoData] = useState(repositoryData);
  const [selectedknownLang,setselectedknownLang] = useState(false);
  const [languages,setLanguages] = useState([]);
  useEffect(() => {
    setRepoData(repositoryData)
    let allLang = [];
    repositoryData?.map((cur)=>{
      allLang.push(cur.language);
    })
    let unique = allLang.filter((item, i, ar) => ar.indexOf(item) === i);
    setLanguages(unique)
  }, [repositoryData]);

  useEffect(async()=>{
    let selected = currentRef.current.value;
    let filterData=[];
    if(selected === "All"){
        dispatch(filterRepoData([]))
    }
    else{
        await repoData?.filter((item) => {
            return item.language === selected
        }).map((cur)=>{
            filterData.push(cur);
        })
        dispatch(filterRepoData(filterData));
    }
},[selectedknownLang])
  return (
    <div className="header-data">
      <div className="listing-header">
        <div className="header-button">
          <span
            className={props.tabName === "repository" ? "btn-header btn" : "btn"}
            onClick={() => {
              history.push("/repository");
            }}
          >
            Repository
          </span>
          <span
            className={props.tabName === "developer" ? "btn-header btn" : "btn"}
            onClick={() => {
              history.push("/developer");
            }}
          >
            Developer
          </span>
        </div>
        <div className="dropdone-nav">
          <ul>
            {props.tabName === "repository" &&
              <li>
                <select
                  ref={currentRef}
                  className="form-select"
                  aria-label="Default select example"
                  onChange={()=>{
                    setselectedknownLang(!selectedknownLang)
                  }}
                >
                  <option selected value ="All">Known Language Any</option>
                  {languages.length >0 &&
                  languages.map((lang)=>{
                    if(lang != null){
                    return(<option value ={lang}>{lang}</option>)}
                  })
                  }
                </select>
              </li>
            }
            <li>
              <select
                className="form-select"
                aria-label="Default select example"
              >
                <option selected>Language Any</option>
                <option value="1">Language</option>
                <option value="2">Language</option>
                <option value="3">Language</option>
              </select>
            </li>

            <li>
              <select
                className="form-select"
                aria-label="Default select example"
              >
                <option selected>Data range Today</option>
                <option value="1">Language</option>
                <option value="2">Language</option>
                <option value="3">Language</option>
              </select>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Header;
