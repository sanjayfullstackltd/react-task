import React, { useEffect, useRef, useState, Fragment } from "react";

import { Link, useHistory } from "react-router-dom";
import Developer from "../Developer/Developer";

function Home(props) {
  const history = useHistory();
  useEffect(() => {
    // Update the document title using the browser API
    // textAnimation();
  });
  return (
    <div>
      <div className="homepage">
        <div className="container-fluid">
          <div className="row">
            <div className="header-block">
              <div className="section-title text-center">
                <h1>Trending</h1>
                <p>
                  See what the Github community is most excited about today.
                </p>
              </div>
            </div>
            <div className="mx-auto col-lg-6">
              <div className="listing-block">
                <div className="listing-header">
                  <div className="header-button">
                    <span onClick={() => history.push("/repository")}>
                      Repository
                    </span>
                    <span onClick={() => history.push("/developer")}>
                      Developer
                    </span>
                  </div>
                  <div className="dropdone-nav">
                    <ul>
                      <li>
                        <select
                          className="form-select"
                          aria-label="Default select example"
                        >
                          <option selected>Open this select menu</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="listing">
                  <Developer />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
