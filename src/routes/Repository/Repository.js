import React, { useEffect, useRef, useState, Fragment } from "react";

import Headers from "./../Header/Header";

import { Link, useHistory } from "react-router-dom";
import Document from "./../../assets/images/document.svg";
import star from "./../../assets/images/star.svg";
import branch from "./../../assets/images/branch.svg";
import userImage from "./../../assets/images/userimage.jpg";
import { useSelector } from "react-redux";
function Repository(props) {
  const history = useHistory();
  const repositoryData = useSelector(state => state.AllData.repoData);
  const FilteredRepositoryData = useSelector(state => state.AllData.filterRepoData);
  const [showData,setShowData] = useState(repositoryData);
  useEffect(() => {
    setShowData(repositoryData)
  }, [repositoryData]);
  useEffect(() => {
    if(FilteredRepositoryData.length > 0){
      setShowData(FilteredRepositoryData)
    }
    else{
      setShowData(repositoryData)
    }
  }, [FilteredRepositoryData]);
  return (
    <div>
      <div className="repository">
        <div className="container-fluid">
          <div className="row">
            <div className="header-block">
              <div className="section-title text-center">
                <h1>Trending</h1>
                <p>
                  See what the Github community is most excited about today.
                </p>
              </div>
            </div>
            <div className="mx-auto col-lg-6">
              <div className="listing-block">
                <Headers tabName="repository" />

                {showData.length > 0 ?
                  showData.map((cur) => {
                    return (
                      <div class="listing">
                        <div class="row">
                          <div className="col-md-8 details">
                            <div className="title">
                              <span className="title-icon">
                                <img src={Document} />
                              </span>
                              <a href={cur.url}>
                                {cur.username} / {cur.repositoryName}{" "}
                              </a>
                            </div>
                            <p>{cur.description}</p>
                          </div>
                          <div className="col-md-4 text-end">
                            <Link className="star">
                              <span className="star-icon">
                                <img src={star} />
                              </span>
                              Star
                            </Link>
                          </div>
                        </div>
                        <div class="row">
                          <div className="col-md-8 lang-details">
                            <span
                              className="lang-name"
                              style={{ color: cur.languageColor }}
                            >
                              {cur.language}
                            </span>

                            <Link className="star">
                              <span className="star-icon">
                                <img src={star} />
                              </span>
                              {cur.starsSince}
                            </Link>

                            <Link className="star">
                              <span className="star-icon">
                                <img src={branch} />
                              </span>
                              {cur.forks}
                            </Link>

                            <span className="lang-name">
                              Build by
                              {cur.builtBy.map((user) => {
                                return (
                                  <span className="userimage">
                                    <img src={user.avatar} />
                                  </span>
                                );
                              })}
                            </span>
                          </div>
                          <div className="col-md-4 text-end star-today">
                            <Link className="star">
                              <span className="star-icon">
                                <img src={star} />
                              </span>
                              {cur.starsSince}
                            </Link>
                          </div>
                        </div>
                      </div>
                    );
                  })
                :
                  <p>Loading...</p>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Repository;
